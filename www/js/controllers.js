angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $http, Tickets) {
  //here we will show tickets owned by the user
  // $http.get('http://192.168.1.14:9999/api/v1/tickets?app_user_id=4')
  //      .then( function ( resp ) {
  //           if( resp.data != null && resp.data.tickets != null ) {
  //               $scope.tickets = resp.data.tickets;
  //           }
  //      });
  // // a scope that post tickets, it serves the form post call from tab-dash.html
  // $scope.addPatient = function ( patient ) {
  //   $http.post({
  //       url: 'http://192.168.1.14:9999/api/v1/add_patient',
  //       data: {
  //         "patient_ids": patient.patient_ids,
  //         "first_vist_array": patient.first_vist_array,
  //         "checkin": patient.checkin
  //       }
  //     }).then( function(resp) {
  //         $scope.tickets.push(resp.data.tickets);
  //     });
  // };
  $scope.postData = {};
  $scope.addPatientToQueue = function () {
      console.log($scope.postData);
      var patient_ids = $scope.postData.patient_ids;
      $http.post('http://192.168.1.14:9999/api/v1/queues/1/add_patient',$scope.postData)
      .then(function(resp) {
        console.log(resp);
        for(var i = 0; i < resp.data.tickets.length ; i++) {
            $scope.tickets.push(resp.data.tickets[i]);
        }
        $scope.$apply();
      });
  }

   Tickets.get({
                    app_user_id: '4'
               },
               //success
               function( resp ) {
                   if( resp.tickets != null ) {
                      $scope.tickets = resp.tickets;
                   }
               },
               //error
               function ( err ) {
                   console.log(err.statusText);
               }
            );



})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };

})

.controller('HomeCtrl', function($scope, $stateParams) {
  $scope.home = {};
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('ClinicsCtrl', function($scope, $http, $ionicLoading) {
  $http.get('http://192.168.1.14:9999/api/v1/clinics')
       .then( function ( resp ) {
            $ionicLoading.hide()
            if(resp.data != null) {
              $scope.clinics = resp.data.data;
            } else {
              $scope.clinics = [{ error : "empty" }];
            }
        }, function ( err ) {
            console.log(err.status);
        });
         console.log($scope.clinics);
  })

.controller('AccountCtrl', function($scope,$http,$ionicLoading) {
  $scope.settings = {
    enableFriends: true
  };

  $http.get('http://192.168.1.14:9999/api/v1/hybrid_version?app_version=1')
       .then(function (resp){
            if(resp.data != null && resp.data.good_to_go != null) {
              if(resp.data.good_to_go == true) {
                $scope.versionCheck = "The version is ok";
              }
             else {
              $scope.versionCheck = "The version is not ok";
            }
          }
       }, function (err) {
         console.log(err);
       }
     );
});
